package com.example.test.web.request;

import com.example.test.common.request.RequestPage;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/19 14:22
 * @Description:
 */
@Data
public class UserPageRequest extends RequestPage {


    private String name;


    @Max(value = 90,message = "年龄不能大于90")
    @Min(value = 10,message = "年龄不能小于90")
    private Integer age;

}
