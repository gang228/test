package com.example.test.web.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/21 13:41
 * @Description: 登录请求体
 */
@Data
public class UserLoginRequest {

    @NotNull(message = "用户名不能为空")
    private String userName;

    @NotNull(message = "密码不能为空")
    private String password;

}
