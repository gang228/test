package com.example.test.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/18 17:15
 * @Description:
 */
@Configuration
public class SwaggerConfig {

    @Value("${swagger.enable}")
    private boolean enable;

    @Bean
    public Docket createApi() {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        parameterBuilder.name("token").description("统一认证token").parameterType("header").required(false).modelRef(new ModelRef("String"));
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(parameterBuilder.build());

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                .enable(enable)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.test.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(parameters);
    }


    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title("Test接口测试")
                .description("测试")
                .version("V1.0")
                .contact(new Contact("gyg", "test.com", "test@test.com"))
                .build();

    }

}
