package com.example.test.common.annocation;

import java.lang.annotation.*;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/21 10:26
 * @Description: 不验证token注解
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface DisableToken {
}
