package com.example.test.common.request;

import lombok.Data;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/19 14:18
 * @Description: 需要分页查询实体集成此类
 */
@Data
public class RequestPage {


    /**
     * 当前页
     */
    private long current;

    /**
     * 每页数量
     */
    private long size;

}
