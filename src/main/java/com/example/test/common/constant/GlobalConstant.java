package com.example.test.common.constant;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/21 11:59
 * @Description: 全局常量
 */
public class GlobalConstant {

    /**
     * 全局token
     */
    public final static String TOKEN = "token";


}
