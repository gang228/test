package com.example.test.common.interceptor;

import com.example.test.common.utils.ServletContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/21 11:21
 * @Description:
 */
@Component
public class ServletContextInterceptor extends ServletContextHolder implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ServletContextHolder.REQUEST.set(request);
        ServletContextHolder.RESPONSE.set(response);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        ServletContextHolder.REQUEST.remove();
        ServletContextHolder.RESPONSE.remove();
    }
}

