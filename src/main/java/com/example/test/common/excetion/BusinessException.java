package com.example.test.common.excetion;

import lombok.Data;

import java.io.Serializable;

/**
 * @author gyg
 * @date 2019/7/2 15:59
 * des 业务异常类
 */
@Data
public class BusinessException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -840821086435906119L;

    private boolean success;
    private String code;
    private String message;
    private Object obj;

    public BusinessException() {
    }

    public BusinessException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public BusinessException(String code, String message, Throwable cause) {
        super(cause);
        this.code = code;
        this.message = message;
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String code, String message, Object obj) {
        super();
        this.code = code;
        this.message = message;
        this.obj = obj;
    }

}
