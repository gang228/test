package com.example.test.common.excetion;

import lombok.CustomLog;
import lombok.Getter;

/**
 * @author gyg
 * @date 2019/7/3 14:26
 * des 异常枚举类
 */
@Getter
public enum ExceptionEnum {

    BIZ_SUCCESS(true,"0000","success"),
    BIZ_FALSE(false,"0001", "服务器繁忙，请稍后再试");

    private boolean success;
    private String code;
    private String message;

    private ExceptionEnum(boolean success,String code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

}
