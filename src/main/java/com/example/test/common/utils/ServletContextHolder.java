package com.example.test.common.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/21 11:12
 * @Description: Servlet上下文
 */
public class ServletContextHolder {

    /**
     * 获取上下文request对象
     */
    protected final static ThreadLocal<HttpServletRequest> REQUEST = new ThreadLocal<>();

    protected final static ThreadLocal<HttpServletResponse> RESPONSE = new ThreadLocal<>();

    public final static HttpServletRequest getRequest(){
        return REQUEST.get();
    }

    public final static HttpServletResponse getResponse(){
        return RESPONSE.get();
    }


}
