package com.example.test.common.utils;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/21 11:03
 * @Description: 上下文用户信息
 */
public class UserHolder {

    protected final static ThreadLocal<UserInfo> USER = new ThreadLocal<>();

    public final static UserInfo getUserInfo(){
        return USER.get();
    }

    protected final static void clear(){
        UserHolder.USER.remove();
    }

}
