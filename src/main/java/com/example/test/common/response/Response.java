package com.example.test.common.response;

import com.example.test.common.excetion.ExceptionEnum;
import lombok.Data;

import java.util.Currency;

/**
 * @author gyg
 * @date 2019/7/3 14:12
 * des 公共返回类型
 */
@Data
public class Response<T> {

    /**
     * 状态码
     */
    private String code;

    /**
     * 消息
     */
    private String message;

    /**
     * 是否成功
     */
    private boolean success;

    /**
     * 响应体
     */
    private T data;

    public Response() {

    }


    public Response(String code) {
        super();
        this.code = code;
    }

    public Response(boolean success,String code, String message) {
        super();
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public Response(boolean success,String code, String message, T data) {
        super();
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Response(ExceptionEnum exceptionEnum) {
        super();
        this.success = exceptionEnum.isSuccess();
        this.code = exceptionEnum.getCode();
        this.message = exceptionEnum.getMessage();
    }

    public Response(ExceptionEnum exceptionEnum, T data) {
        super();
        this.success = exceptionEnum.isSuccess();
        this.code = exceptionEnum.getCode();
        this.message = exceptionEnum.getMessage();
        this.data = data;
    }

    public static <T> Response<T> success() {
        return new Response<T>(ExceptionEnum.BIZ_SUCCESS);
    }


    public static <T> Response<T> success(T data) {
        return new Response<T>(ExceptionEnum.BIZ_SUCCESS, data);
    }

    public static <T> Response<T> fail() {
        return new Response<T>(ExceptionEnum.BIZ_FALSE);
    }

    public static <T> Response<T> fail(T data) {
        return new Response<T>(ExceptionEnum.BIZ_FALSE, data);
    }


}
