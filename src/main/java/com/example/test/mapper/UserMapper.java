package com.example.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.test.entity.User;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/18 18:26
 * @Description:
 */
public interface UserMapper extends BaseMapper<User> {
}
