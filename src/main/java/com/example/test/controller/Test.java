package com.example.test.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.test.common.constant.GlobalConstant;
import com.example.test.common.excetion.BusinessException;
import com.example.test.common.interceptor.ServletContextInterceptor;
import com.example.test.common.response.Response;
import com.example.test.common.utils.*;
import com.example.test.entity.User;
import com.example.test.service.UserService;
import com.example.test.web.request.UserLoginRequest;
import com.example.test.web.request.UserPageRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/18 17:07
 * @Description:
 */
@RestController
@RequestMapping(value = "/test")
@Api(value = "测试类")
public class Test {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/getTest/{id}")
    @ApiOperation(value = "测试方法")
    public Response<User> Test(@PathVariable long id) {
        User user = userService.getUserById(id);
        if(user.getId() == 1){
            throw new BusinessException("403001","参数验证异常");
        }
        UserInfo userInfo = UserHolder.getUserInfo();
        System.out.println("全局获取登录信息：" + userInfo);
        redisTemplate.opsForValue().set("user",user);
        User user1 = (User)redisTemplate.opsForValue().get("user");
        System.out.println(user1);
        return Response.success(user);
    }

    @GetMapping("/getTestSlave/{id}")
    @ApiOperation(value = "测试方法")
    public Response<User> TestSlave(@PathVariable long id) {
        User user = userService.getUserSlaveById(id);
        if(user.getId() == 1){
            throw new BusinessException("403001","参数验证异常");
        }
        return Response.success(user);
    }



    @PostMapping("/pageUser")
    @ApiOperation(value = "分页查询")
    public Response<Page<User>> Test(@RequestBody @Valid UserPageRequest userPageRequest) {
        Page<User> userPage = userService.selectUserPage(userPageRequest);
        return Response.success(userPage);
    }

    @PostMapping("/login")
    @ApiOperation(value = "登录")
    public Response<Void> login(@RequestBody @Valid UserLoginRequest userLoginRequest) {
        String userName = userLoginRequest.getUserName();
        String password = userLoginRequest.getPassword();
        if("admin".equals(password)){
            UserInfo userInfo = new UserInfo();
            userInfo.setUserId("13938076827");
            userInfo.setUserName("gyg");
            String userInfoString = JSON.toJSONString(userInfo);
            String time = DateUtil.formatDate(new Date(),"yyyyMMddHHmmss");
            String key = userInfoString + time;
            String accessToken = AESUtil.getInstance().encrypt(key);
            ServletContextInterceptor.getResponse().setHeader(GlobalConstant.TOKEN, accessToken);
        }
        return Response.success();
    }

}
