package com.example.test.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.test.entity.User;
import com.example.test.web.request.UserPageRequest;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/18 18:38
 * @Description:
 */
public interface UserService {

    /**
     * 通过id查询
     * @param id
     * @return
     */

    User getUserById(Long id);


    /**
     * 备库查询
     * @param id
     * @return
     */

    User getUserSlaveById(Long id);


    /**
     * 分页查询user
     * @param userPageRequest
     * @return
     */
    Page<User> selectUserPage(UserPageRequest userPageRequest);

}
