package com.example.test.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.test.entity.User;
import com.example.test.mapper.UserMapper;
import com.example.test.service.UserService;
import com.example.test.web.request.UserPageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author gyg
 * @version 1.0
 * @date 2020/4/18 18:39
 * @Description:
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @Override
    // @DS(value = "master")
    public User getUserById(Long id) {
        return userMapper.selectById(id);
    }

    /**
     * 备库查询
     *
     * @param id
     * @return
     */
    @Override
    @DS(value = "slave")
    public User getUserSlaveById(Long id) {
        return userMapper.selectById(id);
    }

    /**
     * 分页查询user
     *
     * @param userPageRequest
     * @return
     */
    @Override
    public Page<User> selectUserPage(UserPageRequest userPageRequest) {
        Page page = new Page(userPageRequest.getCurrent(),userPageRequest.getSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(User.NAME,userPageRequest.getName());
        Page<User> userPage = userMapper.selectPage(page, queryWrapper);
        return userPage;
    }


}
